#include <string.h>
#include <stdio.h>

void reverse(char *s,int start,int end) {
    if (s == NULL) {
        return;
    }
    
    if (start >= end || strlen(s) < end) return;
    for(int i=start; i <= (start + end)/2; ++i)
    {
        char tmp = s[i];
        s[i] = s[start + end - i];
        s[start + end - i] = tmp;

    }
}
int main () {
   char str[80] = "This is - www.tutorialspoint.com - website";
   const char s[5] = " -";

   printf("%s address: %p\n",str,str);
   int last = strlen(str)-1;
   reverse(str,0,last);
   printf("%s address: %p\n",str,str);
   
   int pos = 0, curr =0;
   
   while(curr <= last)
   {
       if (strchr(s,str[curr]) != NULL) {
            reverse(str,pos,curr-1);
            ++curr;
            pos = curr;
       } else if (curr==last) {
            reverse(str,pos,curr);
       }
       ++curr;

   }

   printf("%s address: %p\n",str,str);

   
   return(0);
}