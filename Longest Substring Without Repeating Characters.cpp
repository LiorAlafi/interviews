

#include <iostream>
#include <unordered_map>
#include <algorithm>
class A {
public:
    A() {}
    int longestSubstring(std::string s);
    
};


int main()
{
    std::cout << "Hello World!\n";
    A a;
    std::cout << a.longestSubstring("abcabcbb") << std::endl;
    std::cout << a.longestSubstring("bbbbb") << std::endl;

    std::cout << a.longestSubstring("pwwkew") << std::endl;

    std::cout << a.longestSubstring("dvdf") << std::endl;
    std::cout << a.longestSubstring("wobgrovw") << std::endl;


}

int A::longestSubstring(std::string s)
{   
    if (s.size() == 0) {
        return 0;
    }

    int max = 0, right = 0,left=0;
    std::unordered_map<char, int> m;

    for (size_t i = 0; i < s.size(); i++)
    {
        auto pos = m.find(s[i]);
        right = i;
        if (pos == m.end())
        {
           
            m[s[i]] = i;
            continue;
        }
        else {
            max = std::max(max, right - left);
            left = std::max(left, m[s[i]] + 1);
         
           m[s[i]] = i;
        }
    }

    if (right + 1 - left > max)
    {
        max = right + 1 - left;
    }
  
    return max;

}