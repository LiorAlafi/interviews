
int inSubstring(const char *s,int start,int end,char c)
{
    if(start == end) return 1;
    
    while(start < end)
    {
        
        if(s[start] == c) 
        {
                    printf("%c",c);

            return 0;
        }
        ++start;
    }
    
    return 1;
}

int lengthOfLongestSubstring(char * s){
    if (s == NULL) {
        printf("null");
        return 0;
    }
   
    int length = strlen(s);
    int maxLength = 0;
    for(int i=0; i < length; ++i)
    {
      int current = 0;
      for(int j=i;j < length; ++j)
      {
          
        if(inSubstring(s,i,j,s[j])== 0)
        {

           if(j-i >= maxLength)
           {
               maxLength = j-i;
           }
            break;
        } else {
            ++current;
        }
          
        
        
      }
      if (maxLength < current) {
          maxLength = current;
      }
    }
    
    return maxLength;
}

